
# функция, считывающая номер нужного поколения и матрицу с изначальным положением из файла
def read_input(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    m = int(lines[0])
    matrix_lines = lines[1:]
    l_matrix = []
    for line in matrix_lines:
        l_matrix.append(list(map(int, line.strip().split())))
    return m, l_matrix

# функция, распечатывающая матрицу
def matrix_print(matrix):
    for line in matrix:
        print(line)

# функция, подсчитываюшая кол-во соседей у клетки
def count_neighbors(matrix, x, y):
    n = len(matrix)
    directions = [(-1, -1), (-1, 0), (-1, 1), 
                  (0, -1),         (0, 1),
                  (1, -1), (1, 0), (1, 1)] # относительные координаты вокруг определенной клетки
    count = 0
    for dx, dy in directions:
        nx, ny = (x + dx) % n, (y + dy) % n # % даёт гарантию того, что если соседняя клетка выходит за
                                            # пределы сетки, то мы попадаем на противоположную сторону
                                            # сетки
        count += matrix[nx][ny]
    return count

# функция, просчитывающая состояние клеток в определенном поколении
def generation_calculate(matrix):
    n = len(matrix)
    new_matrix = [[0] * n for _ in range(n)]
    for x in range(n):
        for y in range(n):
            neighbors = count_neighbors(matrix, x, y)
            if matrix[x][y] == 0 and neighbors == 3:
                new_matrix[x][y] = 1
            elif matrix[x][y] == 1 and (neighbors == 2 or neighbors == 3):
                new_matrix[x][y] = 1
            else:
                new_matrix[x][y] = 0
    return new_matrix

# функция, записывающая получившуюся матрицу в файл
def write_output(file_path, matrix):
    with open(file_path, 'w') as file:
        for row in matrix:
            file.write(' '.join(map(str, row)) + '\n')

def main():
    m, life_matrix = read_input("data/input.txt")
    print(f"Изначальные данные:\n{m}")
    matrix_print(life_matrix)
    current_gen = life_matrix # создаем отдельную переменную-матрицу для вычислений каждого поколения
    for i in range(m-1):
        current_gen = generation_calculate(current_gen)
    write_output("data/output.txt", current_gen)
    print("Получившаяся матрица записана в файл output.txt")
    
if __name__ == "__main__":
    main()